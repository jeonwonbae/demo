package com.example.demo.global.utils;

public class StringUtils {


    public static boolean isEmpty(String str) {
        return (str == null || "".equals(str));
    }

    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }
}

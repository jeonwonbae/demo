package com.example.demo.domain.board;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Transactional(readOnly = true)
public class BoardService {

    BoardRepository boardRepository;

    public List<Board> list(BoardSearchDTO search){
        return boardRepository.findAll();
    }

    @Transactional
    public Board insert(Board board) {
        return boardRepository.saveAndFlush(board);
    }

    @Transactional
    public Board update(Board board) {
        return boardRepository.saveAndFlush(board);
    }

    @Transactional
    public void delete(Board board) {
        boardRepository.delete(board);
    }
}

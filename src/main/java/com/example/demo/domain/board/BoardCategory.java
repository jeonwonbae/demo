package com.example.demo.domain.board;

import lombok.Getter;

@Getter
public enum BoardCategory {
    NEWS("뉴스"),
    TECH("테크"),
    ETC("기타")
    ;

    String name;

    BoardCategory(String name) {
        this.name = name;
    }
}

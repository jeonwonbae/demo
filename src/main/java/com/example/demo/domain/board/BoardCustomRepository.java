package com.example.demo.domain.board;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BoardCustomRepository {

    Page<Board> findAll(BoardSearchDTO search, Pageable pageable);

    List<Board> findAll(BoardSearchDTO search);

}

package com.example.demo.domain.board;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Board {

    @Id
    String boardId;

    String name;

    @Enumerated(EnumType.STRING)
    BoardCategory boardCategory;

    @CreatedDate
    LocalDateTime createDate;

    @Builder
    public Board(String name, BoardCategory boardCategory) {
        this.boardId = UUID.randomUUID().toString();
        this.name = name;
        this.boardCategory = boardCategory;
    }
}

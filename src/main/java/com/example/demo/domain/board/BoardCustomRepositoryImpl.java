package com.example.demo.domain.board;

import com.example.demo.global.utils.StringUtils;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static com.example.demo.domain.board.QBoard.board;

public class BoardCustomRepositoryImpl extends QuerydslRepositorySupport implements BoardCustomRepository {

    public BoardCustomRepositoryImpl() {
        super(Board.class);
    }

    @Override
    public PageImpl<Board> findAll(BoardSearchDTO search, Pageable pageable) {
        JPQLQuery query = from(board);
        searchBuilder(search, query);
        return new PageImpl<Board>(query.fetch(), pageable, query.fetchCount());
    }

    @Override
    public List<Board> findAll(BoardSearchDTO search) {
        JPQLQuery query = from(board);
        searchBuilder(search, query);
        return query.fetch();
    }

    private void searchBuilder(BoardSearchDTO search, JPQLQuery query) {
        if(StringUtils.notEmpty(search.getName())) {
            query.where(board.name.like(search.getName()));
        }
        if (search.getBoardCategory() != null) {
            query.where(board.boardCategory.eq(search.getBoardCategory()));
        }
        if (search.getCreateDate() != null) {
            query.where(board.createDate.eq(search.getCreateDate()));
        }
        if (search.getCreateDateGoe() != null) {
            query.where(board.createDate.goe(search.getCreateDateGoe()));
        }
        if (search.getCreateDateLoe() != null) {
            query.where(board.createDate.loe(search.getCreateDateLoe()));
        }
    }
}

package com.example.demo.domain.board;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BoardSearchDTO {

    String boardId;
    String name;
    BoardCategory boardCategory;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDateTime createDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDateTime createDateGoe;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDateTime createDateLoe;

}

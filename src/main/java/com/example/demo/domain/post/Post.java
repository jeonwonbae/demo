package com.example.demo.domain.post;

import com.example.demo.domain.board.Board;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Getter
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Post {

    @Id
    String postId;

    String title;

    String content;

    @CreatedDate
    LocalDate createDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "boardId")
    Board board;

    @Builder
    public Post(String title, String content, Board board) {
        this.postId = UUID.randomUUID().toString();
        this.title = title;
        this.content = content;
        this.board = board;
    }
}

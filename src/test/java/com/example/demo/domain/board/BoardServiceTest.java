package com.example.demo.domain.board;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SpringExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BoardServiceTest {

    @Autowired
    BoardRepository boardRepository;

    @Test
    @Rollback(value = false)
    public void insertTest() {
        Board board = Board.builder().boardCategory(BoardCategory.NEWS)
                .name("시사")
                .build();

        boardRepository.saveAndFlush(board);
    }

    @Test
    public void listTest() {
        List<Board> boardList = boardRepository.findAll();

        System.out.println(boardList);

    }

    @Test
    @Rollback(value = false)
    public void deleteTest() {
        List<Board> boardList = boardRepository.findAll();

        boardRepository.delete(boardList.get(0));
    }

}
